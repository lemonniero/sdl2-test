FROM didstopia/sdl2

RUN apt-get update && apt-get upgrade -y
RUN dpkg -l gcc
RUN dpkg -l make
RUN dpkg -l libsdl2-dev
